// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract NFT is ERC721URIStorage {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenId;
    address contractAddress;

    constructor(address marketplaceAddress) ERC721("QUYMINH", "QUY") {
        contractAddress = marketplaceAddress;
    }

    function createToken(string memory tokenURI) public returns(uint256) {
        _tokenId.increment();
        uint256 itemId = _tokenId.current();
        _safeMint(msg.sender, itemId);
        _setTokenURI(itemId, tokenURI);
        setApprovalForAll(contractAddress, true);
        return itemId;
    }
}
