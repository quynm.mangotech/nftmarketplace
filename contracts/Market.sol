//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import '@openzeppelin/contracts/token/ERC721/ERC721.sol';
import '@openzeppelin/contracts/utils/Counters.sol';
import '@openzeppelin/contracts/token/ERC721/IERC721.sol';

contract Market {
    using Counters for Counters.Counter;
    Counters.Counter private _itemIds;
    address payable owner;

    constructor() {
        owner = payable(msg.sender);
    }

    struct Item {
        uint itemId;
        address nftContract;
        uint tokenId;
        address payable owner;
        uint256 price;
        bool onSale;
    }

    mapping(uint256 => Item) private MarketItem_Ids;

    // Add item to the marketplace
    function mintNFT(address _nftContract, uint256 _tokenId, uint _price) public returns(Item memory){
        require(_price > 0, "Price must be at least 1 wei");
        _itemIds.increment();
        uint256 itemId = _itemIds.current();
        Item memory item = Item(itemId, _nftContract, _tokenId, payable(msg.sender), _price, true);
        MarketItem_Ids[itemId] = item;
        return item;
    }
    // Buy NFT
    function buyNFT(address _nftContract, uint256 _itemId) public payable {
        uint256 price = MarketItem_Ids[_itemId].price;
        uint256 tokenId = MarketItem_Ids[_itemId].tokenId;
        require(msg.value == price, "You must pay the price of the item");
        MarketItem_Ids[_itemId].owner.transfer(msg.value - msg.value/10);
        IERC721(_nftContract).safeTransferFrom(MarketItem_Ids[_itemId].owner, msg.sender, tokenId);
        MarketItem_Ids[_itemId].owner = payable(msg.sender);
        MarketItem_Ids[_itemId].onSale = false;
        owner.transfer(msg.value/10);
    }

    // For sale my item
    function forSale(uint itemId) public {
        require(MarketItem_Ids[itemId].owner == msg.sender,  "only owner has privilege to sell this item");
            MarketItem_Ids[itemId].onSale = true;
    }   

    // Fetch list of items for sale
    function fetchNFTs() public view returns (Item[] memory) {
        uint256 itemCount = _itemIds.current();
        uint256 currentIndex = 0;
        Item[] memory items = new Item[] (itemCount);
        for (uint256 i = 1; i< itemCount+1; i++) {
            if(MarketItem_Ids[i].onSale == true) {
                items[currentIndex] = MarketItem_Ids[i];
                currentIndex++;
            }
        }
        return items;
    }   

    // Fetch my list of items
    function fetchMyNfts() public view returns (Item[] memory) {
        uint256 itemCount = 0;
        uint256 currentIndex = 0;
        uint256 totalItems =_itemIds.current();
        for (uint256 i = 1; i <= totalItems; i++) {
            if(MarketItem_Ids[i].owner == msg.sender) {
                itemCount += 1;
            }  
        }
        Item[] memory items = new Item[](itemCount);
        for (uint i =1; i<= totalItems; i++) {
            if(MarketItem_Ids[i].owner == msg.sender) {
                items[currentIndex] = MarketItem_Ids[i];
                currentIndex++;
            }
        }
        return items;
    }
}