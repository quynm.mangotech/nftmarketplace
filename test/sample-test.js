const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Market", function() {
  it("Should create and execute marketplace sales", async function() {
    const MaketRef = await ethers.getContractFactory("Market");
    const market = await MaketRef.deploy();
    const mContrat = await market.deployed();
  

    const NFTRef = await ethers.getContractFactory("NFT");
    const nft = await NFTRef.deploy(mContrat.address);
    const nftContract = await nft.deployed();


    await nftContract.createToken("https://www.mytoken1location.com");
    await nftContract.createToken("https://www.mytoken2location.com");
    await nftContract.createToken("https://www.mytoken3location.com");


    await mContrat.mintNFT(nftContract.address,1,ethers.utils.parseEther(String(10)));
    await mContrat.mintNFT(nftContract.address,2,ethers.utils.parseEther(String(100)));
    await mContrat.mintNFT(nftContract.address,3,ethers.utils.parseEther(String(100)));


    const [_, buyerAddress] = await ethers.getSigners();
    await mContrat.connect(buyerAddress).buyNFT(nftContract.address, 1, {value: ethers.utils.parseEther(String(10))});

    const items = await mContrat.fetchNFTs();
    const myitems = await mContrat.fetchMyNfts();
    // console.log("items: ",myitems)

    console.log("items: ",myitems)

    expect(items.length).to.equal(3);
  })
})